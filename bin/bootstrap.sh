#!/bin/bash
#
# Shell script to bootstrap a new machine
#
#
#
#
#
netcat=nc
timeout=3
PROXY=proxy

#RH5 nc in /usr/bin
if [[ -f /usr/bin/nc ]] ;  then
    netcat=/usr/bin/nc
fi

#Start in $HOME
cd $HOME

echo "Test Proxies? (Yes or No)"
read answer
if [[ $answer = [Yy][Ee][Ss] ]] ; then
  #Set Proxies if needed
  if ${netcat} -w ${timeout} -z ${PROXY} 80 >/dev/null 2>&1; then
    echo "Setting Proxies"
    export HTTP_PROXY=http://proxy #default to port 80
    export HTTPS_PROXY=http://proxy:8080
    export SOCKS5_SERVER=socks #default to port 1080
  else
    echo "Proxies not found ... Not setting Proxies"
  fi
else
  echo "Proxies will not be tested ... Not setting Proxies"
fi

if git --version >/dev/null 2>&1; then
  echo "Git Found."
else
  echo "Git not found.  Please intall git and rerun this script."
  exit 1
fi

if [[ -d $HOME/tmp ]] ; then
  echo "$HOME/tmp found"
else
  echo "Creating $HOME/tmp"
  mkdir $HOME/tmp
fi

if [[ -f /usr/bin/mr ]] ; then
  echo "Found mr"
else
  if [[ -d $HOME/tmp/myrepos ]] ; then
    echo "Found $HOME/tmp/myrepos."
  else
    echo "Cloning myrepos"
    git clone https://git.joeyh.name/git/myrepos.git $HOME/tmp/myrepos
  fi
  export PATH=$PATH:$HOME/tmp/myrepos:
fi

if [[ -f /usr/bin/vcsh ]] ; then
  echo "Found vcsh"
else
  if [[ -d $HOME/tmp/vcsh ]] ; then
    echo "Found $HOME/tmp/vcsh."
  else
    echo "Cloning vcsh"
    git clone https://github.com/RichiH/vcsh.git $HOME/tmp/vcsh
  fi
  export PATH=$PATH:$HOME/tmp/vcsh:
fi

if [[ -d $HOME/.config/vcsh/repo.d/mr.git ]] ; then 
  echo "Found vcsh-mr repo"
else
  echo "Cloning vcsh-mr repo"
  vcsh clone https://bitbucket.org/arndtc/vcsh-mr.git mr
fi

if [[ -d $HOME/.config/vcsh/repo.d/ssh.git ]] ; then
  echo "Found vcsh-ssh repo"
else
  echo "Cloning vcsh-ssh repo"
  vcsh clone https://bitbucket.org/arndtc/vcsh-ssh.git ssh
fi

if [[ -f $HOME/.ssh/id_rsa_bb ]] ; then
  echo "Found id_rsa_bb ssh key."
  echo "Testing BB access."
  ssh git@bitbucket.org
  echo "Did BB ssh testing produce any errors? (Yes or No)"
  read answer
  if [[ $answer = [Yy][Ee][Ss] ]] ; then
    echo "Verify id_rsa_bb ssh key and rerun."
    exit 1
  fi
else
  echo "Did not find id_rsa_bb ssh key. Please generate key and rerun."
  exit 1
fi

echo "Test GL access and keys? (Yes or No)"
read answer
if [[ $answer = [Yy][Ee][Ss] ]] ; then
  if [[ -f $HOME/.ssh/id_rsa_gl ]] ; then
    echo "Found id_rsa_gl ssh key."
    echo "Testing GL access."
    ssh git@gitlab.com
    echo "Did GL ssh testing produce any errors? (Yes or No)"
    read answer
    if [[ $answer = [Yy][Ee][Ss] ]] ; then
      echo "Verify id_rsa_gl ssh key and rerun."
      exit 1
    fi
  else
    echo "Did not find id_rsa_gl ssh key. Please generate key and rerun."
    exit 1
  fi
fi

echo "Did cloning produce any errors? (Yes or No)"
read answer
if [[ $answer = [Nn][Oo] ]] ; then
  echo "Enter Frist Name, Last Name and Email (FN LN EMAIL):"
  read FN LN EMAIL 
  echo "Updating mr git config"
  vcsh mr remote set-url origin ssh://git@bitbucket.org/arndtc/vcsh-mr.git
  vcsh mr config user.name "$FN $LN"
  vcsh mr config user.email "$EMAIL"

  echo "Updating ssh git config"
  vcsh ssh remote set-url origin ssh://git@bitbucket.org/arndtc/vcsh-ssh.git
  vcsh ssh config user.name "$FN $LN"
  vcsh ssh config user.email "$EMAIL"

  echo "Running myrepos update"

  if [[ -d $HOME/bin ]] ; then
    echo "Found $HOME/bin"
  else
    echo "Creating $HOME/bin"
    mkdir $HOME/bin
  fi

  echo "Backup bash files before initial cloning ..."
  if [[ -f $HOME/.bash_logout ]];  then mv --backup=numbered $HOME/.bash_logout $HOME/.bash_logout.bak; fi
  if [[ -f $HOME/.bash_profile ]]; then mv --backup=numbered $HOME/.bash_profile $HOME/.bash_profile.bak; fi
  if [[ -f $HOME/.bashrc ]]; then mv --backup=numbered $HOME/.bashrc $HOME/.bashrc.bak; fi
  if [[ -f $HOME/.profile ]]; then mv --backup=numbered $HOME/.profile $HOME/.profile.bak; fi
  if [[ -f $HOME/.login ]]; then mv --backup=numbered $HOME/.login $HOME/.login.bak; fi

  mr up
else
  echo "Fix any cloning errors and rerun"
  exit 1
fi

echo "Don't forget to update config for any repos you cloned"
echo "vcsh ssh config user.name \"$FN $LN\""
echo "vcsh ssh config user.email \"$EMAIL\""

exit 0
